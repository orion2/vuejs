
import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

import axios from 'axios'
import router from '../../router'

export default new Vuex.Store({
  state: {
    pages: { },
    active: {
      now: '',
      elm: null
    },
    grid: {},
    yaml: `
    project: espace cinko
    city: paris
    version: 0.0.1
    developer: 'q4Zar <pierredamiendelbreil@gmail.com>'
    `
  },
  mutations: {
    __init__(state){
      const yaml = require('js-yaml')
      state.grid = yaml.safeLoad(state.yaml).grid
    },
    ApiContactCreate(state, payload){
      axios.post('https://api.aendlyx.tech/contacts', payload)
      .then(rsp=>{
      })
      router.push('/')
    },
    over(state, event){
      // let idnum = event.target.id.split('-')[2]
      // if (idnum) { //> sidenav or homenav
      //   let targets = [`grid-sidenav-${idnum}`, `grid-homenav-${idnum}-ico`, `grid-homenav-${idnum}-txt`]
      //   targets.forEach(element=>{
      //     let domitem = document.getElementById(element)
      //     //console.log(domitem)
      //     domitem.style.backgroundColor = '#262626'
      //     domitem.style.color = '#04BF8A'
      //   })
      // }
      // else {
      //   let domitem = document.getElementById(event.target.id)
      //   //console.log(domitem)
      //   domitem.style.backgroundColor = '#262626'
      //   domitem.style.color = '#04BF8A'
      // }
    },
    leave(state, event){
      // let idnum = event.target.id.split('-')[2]
      // if (idnum) { //> sidenav or homenav
      //   let targets = [`grid-sidenav-${idnum}`, `grid-homenav-${idnum}-ico`, `grid-homenav-${idnum}-txt`]
      //   targets.forEach(element=>{
      //     let domitem = document.getElementById(element)
      //     //console.log(domitem)
      //     domitem.style.backgroundColor = '#403F3D'
      //     domitem.style.color = '#BFBCBA'
      //   })
      // }
      // else {
      //   let domitem = document.getElementById(event.target.id)
      //   //console.log(domitem)
      //   domitem.style.backgroundColor = '#403F3D'
      //   domitem.style.color = '#BFBCBA'
      // }
    },
    clicked(state, event){

      state.active.now = event.target.id

      let className = event.target.parentNode.className.slice(0,9)
      let page = document.getElementById(event.target.id)
      console.log(page)
      page.style.active = 'true'
      // let homenav = null

      // if (event.target.id in state.pages) {

      //   console.log(event.target.id)

      // } else {

      //   state.pages[event.target.id]=page
      //   console.log(state.pages[event.target.id])

      // }

      // if ( className === 'grid-topn' || className === 'grid-side' || className === 'grid-home' || event.target.id === 'private_collection' ){

      //   // deactive homenav
      //   console.log(className)
      //   let elm = document.getElementsByClassName('grid-homenav')
      //   console.log(elm)
      //   elm[0].style.display = 'none'

      //   // store it to re-display
      //   if (state.active.elm === null){
      //     // needed to reactivate
      //     state.active.elm = elm[0]
      //   }

      //   homenav = 'none'

      // } else if ( className === 'grid-topb' )  {

      //   // reactive homenav
      //   state.active.elm.style.display = 'grid'
      //   homenav = 'grid'

      // } else {
      //   state.active.elm.style.display = 'none'
      //   homenav = 'none'
      // }



      // console.log(state.active.now)
      // console.log(state.pages[state.active.now])
      // console.log(state.pages)

    }
  },
  getters: {
    homenavobjs(state){
      let objs = []
      let idxs = [0,1,2,3,4,5,6,7]
      idxs.forEach(i => {
        objs.push({
          ico: state.grid.sidenav.content[i],
          txt: state.grid.homenav.content[i],
        })
      })
      // console.log(objs)
      return objs
    }
  }
})
